package inheritance;

public class BookStore {
    public static void main(String[] args) {
        book[] books = new book[5];

        books[0] = new book("Da Vinci Code", "Dan Brown");
        books[1] = new ElectronicBook("Harry Potter", "J.K. Rowling", 7.6);
        books[2] = new book("To Kill a Mockingbird", "Harper Lee");
        books[3] = new ElectronicBook("Lord of the Rings", "J. R. R. Tolkien", 8.2);
        books[4] = new ElectronicBook("Twilight", "Stephenie Meyer", 4.5);

        for (int i = 0; i < books.length; i++) {

            System.out.println(books[i]);

        }

    }
}
