package inheritance;

public class book {
    protected String title;
    private String author;

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public String toString() {
        return "Book Name: " + this.title + ", Author Name: " + this.author;
    }
}
