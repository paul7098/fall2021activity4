package inheritance;

public class ElectronicBook extends book {
    public double numberBytes;

    public double getNumberByters() {
        return numberBytes;
    }

    public ElectronicBook(String title, String author, double numberBytes) {
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public String toString() {
        return "Book Name: " + this.title + ", Author Name: " + this.getAuthor() + ", Size: " + this.numberBytes
                + " MB";
    }

}
